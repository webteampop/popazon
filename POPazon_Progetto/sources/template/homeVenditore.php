<div class="row">
      <div class="col-md-2"></div>
      <div class="col-12 col-md-8">
        <div class="list-group shadow">
          <a href="gestisciProdotti.php?action=1" class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="text-center">
              <h2 class="mb-1 ">Aggiungi nuovo prodotto</h2>
              <img src="<?php echo UPLOAD_DIR;?>icone/Add-icon.png" alt=""/>
            </div>
          </a>
          <a href="visualizzaProdottiVenditore.php" class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="text-center">
              <h2 class="mb-1">Visualizza prodotti</h2>
              <img src="<?php echo UPLOAD_DIR;?>icone/Ecommerce-Product-icon.png" alt=""/>
            </div>
          </a>
          <a href="gestisciOrdiniVenditore.php" class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="text-center">
              <h2 class="mb-1">Gestisci ordini</h2>
              <img src="<?php echo UPLOAD_DIR;?>icone/Ecommerce-Handle-With-Care-icon.png" alt=""/>
            </div>
          </a>
          <a href="gestisciCategorieVenditore.php" class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="text-center">
              <h2 class="mb-1">Gestisci Categorie</h2>
              <img src="<?php echo UPLOAD_DIR;?>icone/Folder-Archive-icon.png" alt=""/>
            </div>
          </a>
        </div>
      </div>
      <div class="col-md-2"></div>
    </div>