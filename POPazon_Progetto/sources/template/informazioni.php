<div class="row">
    <div class="col-12 text-center">
        <h2>Informazioni e contatti</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-12 col-md-8">

        <div class="card mb-3 shadow">
            <h4 class="card-header" id="chiSiamo">Chi siamo</h5>
                <div class="card-body">
                    <p class="card-text">
                        Benvenuto su PopAzon, un e-commerce dedicato alla vendita di "Funko Pop!", action figure in vinile da collezione. 
                        Nel nostro sito potete trovare prodotti esclusivi, distribuiti da Funko dapprima in noti store americani ed importati in seguito in Europa. 
                        Per restare sempre aggiornaro riguardo alla disponibilità dei prodotti, iscriviti alla nostra newsletter!
                    </p>
                </div>
        </div>

        <div class="card mb-3 shadow">
            <h4 class="card-header" id="contatti">Contatti</h5>
                <div class="card-body">
                    <p class="card-text">
                        Qui a PopAzon miriamo a fornire un livello eccezionale di servizio ed a superare sempre le vostre aspettative. Potete contattarci tramite posta elettronica, all'indirizzo: <a href="mailto:<?php echo $templateParams["info"]["email"] ?>"><?php echo $templateParams["info"]["email"] ?></a>
                        Tentiamo di rispondere a tutte le email entro 1 giorno lavorativo. L'utilizzo del centro messaggi consente a voi e al nostro team di tenere traccia di tutte le query e degli ordini in un unico luogo.
                    </p>
                    <p class="card-text">
                        Telefono: <?php echo $templateParams["info"]["cel"] ?>
                    </p>
                    <p class="card-text">
                        Indirizzo: <?php echo $templateParams["info"]["via"] ?>, <?php echo $templateParams["info"]["numero"] ?>, <?php echo $templateParams["info"]["citta"] ?>, <?php echo $templateParams["info"]["cap"] ?>, <?php echo $templateParams["info"]["provincia"] ?>
                    </p>
                </div>
        </div>

        <div class="card mb-3 shadow">
            <h4 class="card-header" id="privacy">Informativa sulla privacy</h5>
                <div class="card-body">
                    <p class="card-text">PoPazon si impegna a proteggere la privacy dei nostri clienti. Si prega di prendere il tempo di rivedere questo avviso che spiega quali informazioni raccogliamo su di te, come le usiamo e i tuoi diritti. Siamo noi stessi il responsabile del trattamento dei dati personali raccolti tramite o in connessione con www.popazon.it e qualsiasi App associata (il "Sito").</p>

                    <h5 class="card-title">Quali dati personali raccogliamo su di te?</h5>
                    <p class="card-text">
                        Raccogliamo dati personali da te quando li fornisci direttamente e attraverso l'uso del Sito. Queste informazioni possono includere:
                    <ul>
                        <li>
                            Le informazioni che fornisci quando utilizzi il nostro sito (ad esempio, il vostro nome, i dettagli di contatto, genere, recensioni di prodotti e qualsiasi informazione che aggiungi al profilo del tuo account);
                        </li>
                        <li>
                            Informazioni su transazioni e fatturazione, se effettui qualsiasi acquisto da noi o tramite il nostro sito (ad esempio dettagli della carta di credito / debito e informazioni sulla consegna);
                        </li>
                        <li>Registrazioni delle tue interazioni con noi (ad es. Se contatti il nostro servizio clienti, interagisci con noi sui social media);</li>
                        <li>Informazioni fornite dall'utente quando si partecipa a una competizione o si partecipa a un sondaggio;</li>
                        <li>Informazioni raccolte automaticamente, utilizzando i cookie e altre tecnologie di tracciamento (ad esempio quali pagine sono state visualizzate e se si è fatto clic su un collegamento in uno dei nostri aggiornamenti e-mail). Potremmo anche raccogliere informazioni sul dispositivo che usi per accedere al nostro Sito;</li>
                        <li>Altre informazioni necessarie, ad esempio potremmo accedere alla tua posizione se ci dai il consenso.</li>

                    </ul>
                    Se acquisti anche in uno dei nostri negozi, potremmo combinare le informazioni che fornisci in negozio (ad esempio se effettui un acquisto o ti iscrivi alla nostra mailing list in negozio) con le informazioni riportate qui sopra.
                    </p>

                    <h5 class="card-title">Per cosa usiamo questi dati personali?</h5>
                    <p class="card-text">
                        A seconda di come usi il nostro sito, le tue interazioni con noi e le autorizzazioni che ci dai, le finalità per le quali utilizziamo i tuoi dati personali includono:
                    <ul>
                        <li>Per portare a termine il tuo ordine e mantenere il tuo account online.</li>
                        <li>Per gestire e rispondere a qualsiasi domanda o reclamo al nostro servizio clienti.</li>
                        <li>Per personalizzare il Sito e mostrarti i contenuti che pensiamo ti possano interessare maggiormente, in base alle informazioni del tuo account, alla cronologia degli acquisti e alla tua attività di navigazione.</li>
                        <li>Migliorare, mantenere il sito e monitorarne l'utilizzo.</li>
                        <li>Per ricerche di mercato, ad es. potremmo contattarti per un feedback sui nostri prodotti.</li>
                        <li>Per inviarti messaggi di marketing e mostrarti pubblicità mirata, quando abbiamo il tuo consenso o quando siamo autorizzati a farlo.</li>
                        <li>Per motivi di sicurezza, per indagare sulle frodi e laddove necessario per proteggere noi stessi e terze parti.</li>
                        <li>Rispettare i nostri obblighi legali e regolamentari.</li>
                    </ul>
                    </p>
                </div>
        </div>

        <div class="card mb-3 shadow">
            <h4 class="card-header" id="cookie">Cookie</h5>
                <div class="card-body">
                    <h5 class="card-title">Cosa sono i cookie e le tecnologie analoghe?</h5>
                    <p class="card-text">I cookie sono piccoli file di testo memorizzati sul computer dell'utente dai siti Internet visitati. Il nostro sito e i nostri partner usano i cookie per migliorare l'esperienza di navigazione, analizzare il traffico di utenti e a fini di personalizzazione e misurazione.

                        È possibile limitare o bloccare i cookie tramite le impostazioni del browser. Le modalità sono illustrate in maggiore dettaglio di seguito, alla sezione “Modalità di gestione dei cookie&rdquo.

                        Inoltre, usiamo pixel di monitoraggio per le nostre e-mail di marketing. Tali pixel ci permettono di sapere se e quando l'e-mail è stata aperta. Usiamo tali informazioni per comprendere meglio in che modo gli utenti interagiscono con i nostri contenuti e per analizzare l'efficacia delle nostre campagne di marketing.

                        È possibile disabilitare i pixel di monitoraggio modificando le impostazioni del proprio account e-mail per bloccare il caricamento o la visualizzazione di immagini esterne.</p>

                    <h5 class="card-title">Modalità di gestione dei cookie</h5>
                    <p class="card-text">Alcuni cookie sono strettamente necessari per permettere all'utente di usare il sito Internet e le sue funzionalità, ad esempio per inserire articoli nel carrello. Senza questo tipo di cookie, tali opzioni non possono essere offerte, compromettendo potenzialmente la funzionalità del sito Internet, ossia l'utente non sarà in grado di effettuare acquisti sul nostro sito.

                        È possibile autorizzare il sito Internet a memorizzare sul proprio dispositivo i cookie selezionati modificando le impostazioni del browser. Ogni browser consente di limitare o bloccare in vario modo i cookie.</p>

                </div>
        </div>


    </div>
    <div class="col-md-2">
        <a id="back-to-top" href="#" class="btn btn-light btn-lg back-to-top" role="button">
            <span class="fas fa-chevron-up"></span>
        </a>
    </div>
</div>