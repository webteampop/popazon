# README #

Di seguito le istruzioni per utilizzare il sito PoPazon.

### CREDENZIALI DB ###

Per impostare le credenziali del proprio stack lamp/wamp/mamp aprire il file config.json e modificare i campi:

* servername
* username
* password
* port

### IMPORTARE DB SU LOCALHOST ###
Collegarsi al seguente link [import sql](http://localhost/phpmyadmin/server_import.php), 
scegliere come file popazon.sql contenuto nella cartella database e cliccare su "esegui"

### CREDENZIALI UTENTI INSERITI ###

* VENDITORE: 
	* email--> admin@email.com, pw--> amministratore
* UTENTI: 
	* email --> plinio.folliero@email.com, pw--> folliero
	* email --> ginopino@email.com, pw--> ginopino
